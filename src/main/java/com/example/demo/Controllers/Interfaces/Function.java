package com.psSoft.WebScrapper.Controllers.Interfaces;

import org.springframework.web.bind.annotation.GetMapping;

public interface Function {

    @GetMapping("/")
    public String readFunction();
}
